package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.json.StringEscapeUtils
import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class featureE2E {


	@Then("User want click product")
	public void user_want_click_product() {
		WebUI.click(findTestObject('featureRegister/Page_Homepage/product'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click button negotiable)")
	public void user_click_button_negotiable() {
		WebUI.callTestCase(findTestCase('buyer/Page_Product Detail/Click Saya Tertarik Nego'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered username {string}")
	public void user_input_registered_username(String username) {
		WebUI.callTestCase(findTestCase('login/User login/Input Username'), [('username') : username ], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered password {string}")
	public void user_input_registered_password(String password) {
		WebUI.callTestCase(findTestCase('login/User login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on LOGIN button")
	public void user_click_on_LOGIN_button() {
		WebUI.callTestCase(findTestCase('login/User login/Click Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User want to click product")
	public void user_want_to_click_product() {
		WebUI.click(findTestObject('featureRegister/Page_Homepage/product'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input price {string}")
	public void user_input_price(String harganego) {
		WebUI.callTestCase(findTestCase('buyer/Page_Product Nego/Input Harga'), [('harganego') : harganego], FailureHandling.STOP_ON_FAILURE)
		//		WebUI.setText(findTestObject('buyer/Page_Product_Nego/textfield_nego_nominal'), harganego)
	}

	@Then("User click button kirim")
	public void user_click_button_kirim() {
		WebUI.callTestCase(findTestCase('buyer/Page_Product Nego/Click Kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User want to click icon profile")
	public void user_want_to_click_icon_profile() {
		WebUI.click(findTestObject('buyer/Page_Homepage_with_Login/nav_profile'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User want to click logout")
	public void user_want_to_click_logout() {
		WebUI.click(findTestObject('featureLogin/pageLogout/logout'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click masuk")
	public void user_click_masuk() {
		WebUI.click(findTestObject('featureLogin/Page_login/button_masuk1'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click icon menu")
	public void user_click_icon_menu() {
		WebUI.callTestCase(findTestCase('edit/pageEditProfile/Click Bar Icon'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click diminati")
	public void user_click_diminati() {
		WebUI.callTestCase(findTestCase('seller/pageDaftarJual/clickDiminati'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click product")
	public void user_click_product() {
		WebUI.callTestCase(findTestCase('seller/pageDaftarJual/clickProduct'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User click terima")
	public void user_click_terima() {
		WebUI.click(findTestObject('featureSeller/pageInfoPenawaran/button_Terima'), FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User click icon profile")
	public void user_click_icon_profile() {
		WebUI.click(findTestObject('buyer/Page_Homepage_with_Login/nav_profile'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click Profile")
	public void user_click_Profile() {
		WebUI.click(findTestObject('featureLogin/pageLogout/profile'), FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input nama {string}")
	public void user_input_nama(String name) {
		WebUI.callTestCase(findTestCase('edit/pageEditProfile/Input Nama'), [('name') : ''], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click submit")
	public void user_click_submit() {
		WebUI.callTestCase(findTestCase('edit/pageEditProfile/Click Submit'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
