 
Feature: E2E
  As a user, I want to add product to cart.
  As a user buyer, I want to login then add product to cart and negotiation product.
  As a user seller, I want to login then approve product.

  @U001
  Scenario: U001 - User want to add product without login
    Then User want click product
    Then User click button negotiable

  @LGI001
  Scenario: LGI001 - User want to login using correct credential (User buyer)
    Then User input registered username "archiveofme10@gmail.com"
    Then User input registered password "students1234"
    Then User click on LOGIN button
    
  @UAP001              
  Scenario: UAP001 - User want to add product to negotiable
  	Then User want to click product
  	Then User click button negotiable
  	Then User input price "200000"
  	Then User input price "10000000"
  	Then User click button kirim
  	
  @LGO001
  Scenario: LGO001 - User buyer want to logout
  	Then User want to click icon profile
  	Then User want to click logout
  	
  @LGS001
  Scenario: LGS001 - User want to login using correct credential (User seller)
  	Then User click masuk
  	Then User input registered username "its211099@gmail.com"
    Then User input registered password "students1234"
    Then User click on LOGIN button
    
  @US001
  Scenario: US001 - User want to approve product
  	Then User click icon menu
  	Then User click diminati
  	Then User click product
  	Then User click terima
  	
  @USE002
  Scenario: USE003 - User seller want to see profile
  	Then User click icon profile
  	Then User click Profile
  	
  @USE003
  Scenario: USE003 - User want to edit name
  	Then User input nama "ganti nama"
  	Then User click submit
  	
  @LGS002
  Scenario: LGS002 - User seller want to logout
  	Then User want to click icon profile
  	Then User want to click logout
