import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('stepDefinition/addProduct'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Click Bar Icon'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Click Edit Button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('featureEdit/Edit Profile/textfield_name'), '')

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Select Kota'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Verify Dropdown Kota'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Input Alamat'), [('address') : 'Jalan BSD Grand Boulevard, Sampora, BSD, Tangerang, Banten 15345. BSD Green Office Park. Tangerang, Banten 12730'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Input Nomor Telepon'), [('phoneNumber') : '6287883021759'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Click Submit'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('edit/pageEditProfile/Read Error Message'), [('expected') : 'Berhasil update profile'], 
    FailureHandling.STOP_ON_FAILURE)

