import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.awt.Rectangle as Rectangle
import javax.wsdl.Import as Import
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.Rectangle as Rectangle

WebUI.callTestCase(findTestCase('stepDefinition/addProduct'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageProduct/verifyMatch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageProduct/clickJual'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/verifyMatch'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/inputNamaProduk'), [('namaProduk') : 'kain tenun'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/inputHargaProduk'), [('hargaProduk') : '1000000\r\n'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/inputDeskripsiProduk'), [('deskripsiProduk') : 'test deskripsi produk\r\n'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/clikKategori'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/verifyMatchDropdownKategori'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/clickKategoriBaju'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageJual/uploadImage'), [('file') : 'C:\\Users\\senoa\\Katalon Studio\\webSecondHand\\Images\\download.jfif'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.takeFullPageScreenshotAsCheckpoint('//nav[@class=\'navbar navbar-expand-lg sticky-top\']/div[@class=\'container\']')

WebUI.callTestCase(findTestCase('seller/pageJual/clickPreview'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('login/logout/logout'), [:], FailureHandling.STOP_ON_FAILURE)

