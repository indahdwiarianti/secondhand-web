import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Step ini gagal dikarenakan dalam menentukan lokatornya menggunakan xpath dan kondisi barang sudah dilakukan pengetesan untuk dijual\r\n'
WebUI.callTestCase(findTestCase('stepDefinition/addProduct'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('seller/pageProduct/clickProductOwn'), [:], FailureHandling.STOP_ON_FAILURE)

//WebUI.scrollToElement(findTestObject('featureSeller/pageProduct/div_productOwn'), 1)
WebUI.verifyElementVisible(findTestObject('featureSeller/pageProduct/div_productOwn'))

//WebUI.getAttribute(findTestObject('featureSeller/pageProduct/div_productOwn'), '')
WebUI.callTestCase(findTestCase('seller/pageProduct/clickProductOwn'), [:], FailureHandling.STOP_ON_FAILURE)

//WebUI.callTestCase(findTestCase('seller/pageProductSeller/verifyContentOwnProduct'), [('expected') : 'Hapus'], FailureHandling.STOP_ON_FAILURE)
WebUI.callTestCase(findTestCase('login/logout/logout'), [:], FailureHandling.STOP_ON_FAILURE)

