import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/logo'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/nav_daftar_jual_saya'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/nav_notifikasi'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/nav_profile'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/img_banner'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/btn_Jual'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/button_Semua'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/button_Kesehatan'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/button_Kendaraan'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/button_Hoby'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/button_Elektronik'))

WebUI.verifyElementVisible(findTestObject('buyer/Page_Homepage_with_Login/button_Baju'))

