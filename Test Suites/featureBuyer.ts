<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>featureBuyer</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f62b92a2-1a15-4289-9739-83864b2ad81b</testSuiteGuid>
   <testCaseLink>
      <guid>f6ab4c09-cea3-49b5-afbe-2ac9f27cd98d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/stepDefinition/userBuyer/UBP006 - User want to add product to cart without login account</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>384e9f8f-9857-4ed2-a439-21f86a0627e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/stepDefinition/userBuyer/UBP001 - User want to login using correct credential as buyer</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c768a8ef-d116-46de-908b-d0f8f0dfce9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/stepDefinition/userBuyer/UBP002 - User want to view product on website</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0d1f49d1-5700-4e36-9d15-ef6a430f8cd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/stepDefinition/userBuyer/UBP003 - User want to view product in category on website</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b59005cd-05ba-4938-b92b-c7c8b983c9b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/stepDefinition/userBuyer/UBP004 - User want to add product on website</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>68b3ac89-4ca9-4680-a152-a191af1a9dd5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>67a7a028-76c9-4766-acf7-55c30b6c8bb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/stepDefinition/userBuyer/UBP005 - User want to add product with negative price on website</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9310fd8c-fc45-436b-b6f5-ef6408ca729d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
