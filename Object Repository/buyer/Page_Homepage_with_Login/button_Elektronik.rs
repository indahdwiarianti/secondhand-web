<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Elektronik</name>
   <tag></tag>
   <elementGuidId>8584634a-add8-4302-ab1e-40fb13c69855</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-color-theme pl-3 pr-3' and (text() = ' Elektronik' or . = ' Elektronik')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>25f874ac-3a92-47b4-99e4-10858fee3320</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4eeced3d-efd9-4e45-8935-b6ca0156fd80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-color-theme pl-3 pr-3</value>
      <webElementGuid>6b393103-7c3d-4d1c-9afa-559b50522b06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Elektronik</value>
      <webElementGuid>2b0a6248-8285-4d76-ac8c-e70f2e292cf8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;pb-5&quot;]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;category&quot;]/div[@class=&quot;category__list&quot;]/button[@class=&quot;btn btn-color-theme pl-3 pr-3&quot;]</value>
      <webElementGuid>0d84d8c3-f847-4bf9-9f41-4fa99a7d5132</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[7]</value>
      <webElementGuid>0ba6c26f-0245-43ef-a883-3217cf1ea1b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/div/button[5]</value>
      <webElementGuid>18e277ed-c72f-4779-8ded-8aef3e3b3ac0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jual'])[1]/preceding::button[2]</value>
      <webElementGuid>b31615cd-3955-4381-97ae-f375c9a943db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Elektronik']/parent::*</value>
      <webElementGuid>9a0c507e-c6d2-491f-b472-c150bec7afd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[5]</value>
      <webElementGuid>9ef68c9a-a5cb-485b-8eb1-5ade1286167d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = ' Elektronik' or . = ' Elektronik')]</value>
      <webElementGuid>45b9f277-81b1-47f3-92a5-f8dc189ff689</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
