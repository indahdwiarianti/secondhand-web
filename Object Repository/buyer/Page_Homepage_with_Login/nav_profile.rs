<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nav_profile</name>
   <tag></tag>
   <elementGuidId>b6607975-bc25-4c4d-8415-2a6913c3186c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#user</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='user']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4029f63c-4ee9-4d74-b33a-75eb5b40f238</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user</value>
      <webElementGuid>cb37e196-cd72-45c1-90d0-00c10662abde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ae644563-2747-4b66-b593-6f63c0c02f0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-link</value>
      <webElementGuid>8077d002-7b47-4fe7-a7e6-9b42b515c972</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>dba243c9-6812-40ab-9f9f-131a2807008f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-display</name>
      <type>Main</type>
      <value>static</value>
      <webElementGuid>4cfc5184-d9d8-44f8-9acf-2895fdd4395b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>61b27052-b882-4156-8e78-acc1bca7d7e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user&quot;)</value>
      <webElementGuid>8df29698-dc32-43e7-ad52-6fdf19204aa1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='user']</value>
      <webElementGuid>3f541be9-f6fd-4a5c-9435-60c67cce2cc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarText']/ul/li[3]/button</value>
      <webElementGuid>0edb0fac-f914-4d4c-be17-b92b8e974a7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 3.000,00'])[2]/following::button[1]</value>
      <webElementGuid>fe7b9753-f9f7-41f9-89ee-d4c292cdeda6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 1.000,00'])[11]/following::button[1]</value>
      <webElementGuid>536ff206-cf01-43c2-b5e9-0ed02ca796bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/preceding::button[1]</value>
      <webElementGuid>21f02f70-2cde-4ca2-a5d2-613c62a2ece9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log Out'])[1]/preceding::button[1]</value>
      <webElementGuid>a03b1727-55af-420c-b503-95f3e90cbf16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/button</value>
      <webElementGuid>08039828-f277-4b0d-9fc3-02cfc10f935c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'user' and @type = 'button']</value>
      <webElementGuid>0e625687-80ed-4834-9711-11e77a25cc8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
