<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_telusuri_kategori</name>
   <tag></tag>
   <elementGuidId>b12384a1-814f-4339-96e8-bbb8206385e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.category > h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>223a0770-1b0e-44f4-acbc-1d97518e8423</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Telusuri Kategori</value>
      <webElementGuid>3382b4c7-504e-40cd-a965-2418368a0f3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;pb-5&quot;]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;category&quot;]/h3[1]</value>
      <webElementGuid>9b601c91-da18-44fe-930e-20d77b66d982</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/h3</value>
      <webElementGuid>1ee443ed-443f-4487-977e-0260dacf8421</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga 60%'])[1]/following::h3[1]</value>
      <webElementGuid>e93c31ac-ffd0-4ceb-8143-09fd0787ab52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan Ramadan Banyak Diskon!'])[1]/following::h3[2]</value>
      <webElementGuid>8314a9f1-fb39-462d-ad4c-39b887c23ce6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua'])[1]/preceding::h3[1]</value>
      <webElementGuid>e1a9b8b3-e6ea-42b2-b9c3-95835475ecca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Telusuri Kategori']/parent::*</value>
      <webElementGuid>e812de51-d9ee-4ba2-841e-17680a5ab965</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h3</value>
      <webElementGuid>41e34c3e-c2d2-427a-8ed1-3961c9520137</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Telusuri Kategori' or . = 'Telusuri Kategori')]</value>
      <webElementGuid>98b474e2-2bf4-49b7-ab7c-8fe9ffd1f5f2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
