<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>itemproduct</name>
   <tag></tag>
   <elementGuidId>c6903542-db2e-4ee2-bdae-0b89f2d9cc75</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@alt = 'Samsung Galaxy A8']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Samsung Galaxy A8</value>
      <webElementGuid>29314012-e040-4a74-ad4b-35c96c33f366</webElementGuid>
   </webElementProperties>
</WebElementEntity>
