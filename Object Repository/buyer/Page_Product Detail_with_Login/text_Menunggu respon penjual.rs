<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Menunggu respon penjual</name>
   <tag></tag>
   <elementGuidId>f55d7fef-a435-49e7-8569-3597b49007c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.d-flex.flex-column.mt-4</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'd-flex flex-column mt-4']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[3]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6bf8f22d-c83c-444f-b4f6-4cba8e0b51e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex flex-column mt-4</value>
      <webElementGuid>74a0f650-58a7-4d1f-aade-1ec53a96820c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Menunggu respon penjual</value>
      <webElementGuid>05893be6-464b-4d82-a43b-e85dcd65529f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;card product&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;d-flex flex-column mt-4&quot;]</value>
      <webElementGuid>f9bc86ca-88e0-4768-8737-058f19b7859a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[3]/div/div/div</value>
      <webElementGuid>671319c7-0d8d-4aef-8b79-0590fd840116</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 50.000,00'])[1]/following::div[1]</value>
      <webElementGuid>4cce972a-967a-49eb-8b68-462336b9abfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='turtleboy'])[1]/following::div[1]</value>
      <webElementGuid>0b80c02a-a14a-4dc9-98ca-3760a61d5b94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ADIKA DWI ANANDA PUTRA'])[1]/preceding::div[1]</value>
      <webElementGuid>8c26e291-3b08-4e13-a4a7-5b4290d54b88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[3]/div/div/div</value>
      <webElementGuid>400011f3-4ecb-432d-83d1-b606c1df03ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Menunggu respon penjual' or . = 'Menunggu respon penjual')]</value>
      <webElementGuid>d28530e4-5def-429c-bc0c-3809f78ddad2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
