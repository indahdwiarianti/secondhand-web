<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_seller_location</name>
   <tag></tag>
   <elementGuidId>f6d36602-ad03-4586-b373-242d0bccfc48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-body > div > p.card-text</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'card-text']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[3]/div[2]/div/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>66354f26-2336-428c-b682-94b0f271b313</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-text</value>
      <webElementGuid>916c1da7-9599-47ce-a9de-f46e7e6ab327</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bandung</value>
      <webElementGuid>c7a400d9-e901-47d4-9589-e05c487c52a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;card seller&quot;]/div[@class=&quot;card-body&quot;]/div[1]/p[@class=&quot;card-text&quot;]</value>
      <webElementGuid>c6332ddf-3c75-4cf9-ad0a-acb6cb319644</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[3]/div[2]/div/div/p</value>
      <webElementGuid>2e6d4f88-157b-4c07-a681-a7440774a8d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ADIKA DWI ANANDA PUTRA'])[1]/following::p[1]</value>
      <webElementGuid>9cb35e98-1c5d-4123-b8a0-b9a4a450d2bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya tertarik dan ingin nego'])[1]/following::p[1]</value>
      <webElementGuid>cd09b0a7-8d59-41b7-ad28-7e1a292c8b21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::p[1]</value>
      <webElementGuid>cc5378a6-37ef-4588-8f6f-67f6d7cea216</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::p[1]</value>
      <webElementGuid>fbe068e0-8011-4bb4-97ec-74a1ee20b123</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bandung']/parent::*</value>
      <webElementGuid>a010e918-b5db-4477-a051-6f1b6b2907f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div/p</value>
      <webElementGuid>8fac0b97-9d27-4d77-8345-3a7722ef4e68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Bandung' or . = 'Bandung')]</value>
      <webElementGuid>6d78c057-e243-4c59-b7d6-ac24522af67e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
