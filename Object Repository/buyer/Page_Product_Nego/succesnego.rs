<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>succesnego</name>
   <tag></tag>
   <elementGuidId>87a6a01a-0ae2-41f8-8f2b-7a008b9c2b9d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fade position-absolute top-0 start-50 translate-middle alert alert-success show']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fade position-absolute top-0 start-50 translate-middle alert alert-success show</value>
      <webElementGuid>74a80a59-6fcb-4e0f-97d8-93998f7e1fb4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
