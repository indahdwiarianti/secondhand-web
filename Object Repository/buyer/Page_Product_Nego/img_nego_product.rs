<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_nego_product</name>
   <tag></tag>
   <elementGuidId>488944c8-bb76-403f-bc14-5b0b5658df49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.product > div.card-body > img.d-block</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//img[@alt='Product']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>c720584f-2c7c-4b5b-b83f-2c795eb79970</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>http://res.cloudinary.com/dgaeremr2/image/upload/v1669740766/Products-Customers/zciljl8zxc5e95ojbaty.png</value>
      <webElementGuid>5ab54172-f6d8-4795-901a-3a65ba5a7cbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-block</value>
      <webElementGuid>50e2f87b-f015-4766-85c0-1666b3019154</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Product</value>
      <webElementGuid>f0415278-ff42-410f-a81c-62427571e1fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-sm modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;card product&quot;]/div[@class=&quot;card-body&quot;]/img[@class=&quot;d-block&quot;]</value>
      <webElementGuid>21ba6fcd-d154-464a-af0f-702c1896f291</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Product']</value>
      <webElementGuid>9559de52-6289-4b2b-a604-28951d7393d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/div/div/img</value>
      <webElementGuid>68c6168b-4db1-45d7-9503-547afe52d773</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'http://res.cloudinary.com/dgaeremr2/image/upload/v1669740766/Products-Customers/zciljl8zxc5e95ojbaty.png' and @alt = 'Product']</value>
      <webElementGuid>f401d435-76ef-4a66-9f5f-f262405af3c0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
