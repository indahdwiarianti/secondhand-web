<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_nego_detail</name>
   <tag></tag>
   <elementGuidId>1b70f878-a724-4423-99bc-056d4cda0068</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-body > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/following::p[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>a401e455-2ad8-46b4-a3b2-660449e03336</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.</value>
      <webElementGuid>2fe72186-73a8-4199-8c39-6ba7ad28aed8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-sm modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/p[1]</value>
      <webElementGuid>811db89b-c4d3-43c8-aa64-9517b5fbbee1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/following::p[1]</value>
      <webElementGuid>cb9ac183-801a-42d9-979c-e462724723f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::p[1]</value>
      <webElementGuid>04d8d9c0-628e-4a88-a9c7-a84fe9ff01eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='turtleboy'])[2]/preceding::p[1]</value>
      <webElementGuid>60794934-c022-4383-a5e5-e450b30ea14a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Tawar'])[1]/preceding::p[2]</value>
      <webElementGuid>772fc9d2-7c8d-44e5-aa4b-8ef74e5de520</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.']/parent::*</value>
      <webElementGuid>915534de-db9f-4751-9c5d-9532e47f802e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/p</value>
      <webElementGuid>0030a594-920f-441c-97fb-75beef5faa1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.' or . = 'Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.')]</value>
      <webElementGuid>c8864e9a-a476-48ba-99b9-a8fb1755b7c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
