<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_detailProduct</name>
   <tag></tag>
   <elementGuidId>fbc1b03c-4d22-4d72-89bc-a4cb7dfe8f36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-10</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[2]/div[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ee38f771-e866-46d7-aac2-f4a65099092c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-10</value>
      <webElementGuid>a677fdb7-056e-4156-a743-798fe6e2987e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Penawaran produk04 Dec, 3:17Top up MLBBRp 20.000,00Ditawar Rp 10.000,00TolakTerima</value>
      <webElementGuid>ad09e55e-d9f8-47b4-8912-781e7b48be52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;row mt-3&quot;]/div[@class=&quot;col-md-10&quot;]</value>
      <webElementGuid>0b8c47f1-af2b-4630-8cac-6d349e368546</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[2]/div[2]/div[2]</value>
      <webElementGuid>aab81159-918f-4a2d-b4bb-6c5cc8db7e0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Produkmu yang Ditawar'])[1]/following::div[4]</value>
      <webElementGuid>33ca8b56-8ad1-45b9-905b-967c447b7fed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='wafwaf'])[1]/following::div[4]</value>
      <webElementGuid>b57d6a5d-3675-409a-a779-bcc6aa8b2538</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]</value>
      <webElementGuid>bad3a31f-8226-46ef-b21a-74e5f5478fbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Penawaran produk04 Dec, 3:17Top up MLBBRp 20.000,00Ditawar Rp 10.000,00TolakTerima' or . = 'Penawaran produk04 Dec, 3:17Top up MLBBRp 20.000,00Ditawar Rp 10.000,00TolakTerima')]</value>
      <webElementGuid>948abd88-6526-4423-862c-817c251eeec6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
