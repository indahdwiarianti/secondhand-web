<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_product_mobil</name>
   <tag></tag>
   <elementGuidId>5eac0b31-8fd0-49a8-a845-71b7a5bd17d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[2]/div/div[2]/div/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>img.d-block.w-100.h-100</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>1b27de7f-a385-4a88-acd7-0ed65c25a720</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>http://res.cloudinary.com/dgaeremr2/image/upload/v1668340993/Products-Customers/xsw3swbfljacb7jfnheh.jpg</value>
      <webElementGuid>f0bc6e95-02dc-4ba7-8857-29d95f725a70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-block w-100 h-100</value>
      <webElementGuid>e615efce-a255-49bc-9cc4-16ebeb3c83b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Gambar produk</value>
      <webElementGuid>db5b1a35-1f7c-4b52-9279-bccf0d1af636</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;carousel slide carousel-fade&quot;]/div[@class=&quot;carousel-inner&quot;]/div[@class=&quot;active carousel-item&quot;]/img[@class=&quot;d-block w-100 h-100&quot;]</value>
      <webElementGuid>d31a5766-f3d6-4100-90ab-a5aa14189feb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[2]/div/div[2]/div/img</value>
      <webElementGuid>47e98509-1d1e-463f-8829-427743c32715</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Gambar produk']</value>
      <webElementGuid>9f4622e0-0be2-4a5e-a7f3-292a66b2b9c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//img</value>
      <webElementGuid>88264f78-5b62-4141-bda0-d7c6d108e97e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'http://res.cloudinary.com/dgaeremr2/image/upload/v1668340993/Products-Customers/xsw3swbfljacb7jfnheh.jpg' and @alt = 'Gambar produk']</value>
      <webElementGuid>c1faba28-a338-4a85-8ca9-b9fb139f47cd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
