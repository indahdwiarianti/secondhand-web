<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textfield_product_desc</name>
   <tag></tag>
   <elementGuidId>e1434e95-8b7f-40af-8417-ce2701cd7d0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = concat(&quot;DeskripsiA Look Inside Blackpink Jennie&quot; , &quot;'&quot; , &quot;s Custom-Built Porsche Taycan 4S Cross Turismo. Based on the design of the Taycan 4S Cross Turismo.&quot;) or . = concat(&quot;DeskripsiA Look Inside Blackpink Jennie&quot; , &quot;'&quot; , &quot;s Custom-Built Porsche Taycan 4S Cross Turismo. Based on the design of the Taycan 4S Cross Turismo.&quot;))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[2]/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>010634b1-1ee0-4637-91f4-c8551d014e8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
      <webElementGuid>5fb4777a-6c64-4e8b-8c9e-6cb24afc021e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DeskripsiA Look Inside Blackpink Jennie's Custom-Built Porsche Taycan 4S Cross Turismo. Based on the design of the Taycan 4S Cross Turismo.</value>
      <webElementGuid>decd513e-fa4c-4c2b-be9c-01925030e343</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;card description mt-5&quot;]/div[@class=&quot;card-body&quot;]</value>
      <webElementGuid>81ae05fe-750f-4365-8309-4a1713b16d97</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[2]/div[2]/div</value>
      <webElementGuid>61710c18-6b8a-44a2-8252-82e5b9869e58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::div[2]</value>
      <webElementGuid>953272c6-07a0-4691-b7fd-4e3f24d5953f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::div[2]</value>
      <webElementGuid>228f621b-50cc-4c24-bd68-8101fa231bcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mobil Porsche'])[1]/preceding::div[1]</value>
      <webElementGuid>3f648b1b-1b69-4861-b8b1-3445fdc1d146</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div</value>
      <webElementGuid>27c10bf3-812f-4f4a-81fe-f78856bd49a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;DeskripsiA Look Inside Blackpink Jennie&quot; , &quot;'&quot; , &quot;s Custom-Built Porsche Taycan 4S Cross Turismo. Based on the design of the Taycan 4S Cross Turismo.&quot;) or . = concat(&quot;DeskripsiA Look Inside Blackpink Jennie&quot; , &quot;'&quot; , &quot;s Custom-Built Porsche Taycan 4S Cross Turismo. Based on the design of the Taycan 4S Cross Turismo.&quot;))]</value>
      <webElementGuid>950b9836-822b-4e86-a35e-eb4117ac4a10</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
